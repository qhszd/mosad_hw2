//
//  AppDelegate.h
//  mosad_HW2
//
//  Created by 邵震东 on 2021/10/13.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>
@property (strong, nonatomic) UIWindow * window;

@end

