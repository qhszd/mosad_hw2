//
//  DataCell.h
//  mosad_HW2
//
//  Created by 邵震东 on 2021/10/20.
//

#ifndef DataCell_h
#define DataCell_h

@interface DataCell : NSObject
@property(nonatomic,strong)NSString *date;
@property(nonatomic,strong)NSString *lctn;
@property(nonatomic,strong)NSString *clth;
@property(nonatomic,strong)NSString *feel;
@end

#endif /* DataCell_h */
