//
//  DataCell.m
//  mosad_HW2
//
//  Created by 邵震东 on 2021/10/20.
//

#import <Foundation/Foundation.h>
#import "DataCell.h"

@interface DataCell()

@end

@implementation DataCell

- (id) init
{
    self = [super init];
    self.date = @"2021-10-01";
    self.lctn = @"Guangzhou, Guangdong";
    self.clth = @"No values";
    self.feel = @"Happy";
    return self;
}

@end
