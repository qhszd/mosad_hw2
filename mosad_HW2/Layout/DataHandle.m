//
//  DataHandle.m
//  mosad_HW2
//
//  Created by  on 2021/10/20.
//

#import <Foundation/Foundation.h>
#import "DataHandle.h"
#import "DataCell.h"

@interface DataHandle()

@end

@implementation DataHandle
static DataHandle *dataHandle = nil;
+ (instancetype) sharedDataHandle {
    if (nil == dataHandle) {
        dataHandle = [[DataHandle alloc] init];
        DataCell *ex1 = [[DataCell alloc] init];
        DataCell *ex2 = [[DataCell alloc] init];
        dataHandle.arrObj = [[NSArray alloc] initWithObjects: ex1, ex2, nil];
    }
    return dataHandle;
}
@end
