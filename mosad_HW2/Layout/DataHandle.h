//
//  DataHandle.h
//  mosad_HW2
//
//  Created by  on 2021/10/20.
//

#ifndef DataHandle_h
#define DataHandle_h
#import <UIKit/UIKit.h>
#import "DataCell.h"

@interface DataHandle : NSObject
+ (instancetype)sharedDataHandle;
@property(nonatomic,strong)NSArray *arrObj;
//@property(nonatomic,strong)NSMutableArray *arrObj;
@end

#endif /* DataHandle_h */
