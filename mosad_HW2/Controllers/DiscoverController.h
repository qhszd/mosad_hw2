//
//  DiscoverController.h
//  mosad_HW2
//
//  Created by 邵震东 on 2021/10/14.
//

#ifndef DiscoverController_h
#define DiscoverController_h
#import <UIKit/UIKit.h>

//@interface DiscoverController : UITableViewController
@interface DiscoverController : UIViewController<UITableViewDataSource, UITableViewDelegate>
{
    UITableView *tableView;
}
@end

#endif /* DiscoverController_h */
