//
//  UserViewController.m
//  mosad_HW2
//
//  Created by 邵震东 on 2021/10/13.
//

#import <Foundation/Foundation.h>
#import "UserController.h"

@interface UserController()
//@property (nonatomic, strong) UIImageView *imageView;
@end

@interface GradientLayer : CALayer

@end

@implementation UserController

-(void)viewDidLoad
{
    [super viewDidLoad];
    [self setBackView];
    
    // 登录按钮
    UIButton *login = [[UIButton alloc] initWithFrame: CGRectMake(self.view.frame.size.width/2 - 150, self.view.frame.size.height/2 - 150, 300, 300)];
    [login setTitleColor:[UIColor systemBlueColor] forState:UIControlStateNormal];
    
    // 设置按钮颜色、文字、圆角、动作并添加
    [login setBackgroundColor: [UIColor colorWithRed: 0 green: 0 blue: 0 alpha: 0]];
    [login.layer setBorderColor: [UIColor systemGray5Color].CGColor];
    [login.layer setBorderWidth: 2.0];
    [login setTitle:@"Login" forState:UIControlStateNormal];
    [login.layer setCornerRadius: login.layer.frame.size.width / 2];
    [login addTarget:self action: @selector(change:) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview: login];
}

- (void)setBackView
{
    GradientLayer *gradientLayer = [[GradientLayer alloc] init];
    gradientLayer.frame = self.view.bounds;
    [self.view.layer addSublayer:gradientLayer];
}

- (void)setBV : (UIView *) view
{
    GradientLayer *gradientLayer = [[GradientLayer alloc] init];
    gradientLayer.frame = view.bounds;
    [view.layer addSublayer:gradientLayer];
}

- (void)change : (UIButton *) btn
{
    UIView *newPage = [[UIView alloc] initWithFrame: self.view.frame];
    [newPage setBackgroundColor: [UIColor whiteColor]];
    [self setBV: newPage];
    
    // 头像
//    UIView *image = [[UIView alloc] initWithFrame: CGRectMake(self.view.frame.size.width/2 - 100, self.view.frame.size.height/2 - 225, 200, 200)];
//    [image.layer setCornerRadius: image.frame.size.width / 2];
//    [image setBackgroundColor: [UIColor colorWithPatternImage: [UIImage imageNamed: @"IMG_0051"]]];
//    [newPage addSubview:image];
    UIImageView *image = [[UIImageView alloc] initWithImage: [UIImage imageNamed:@"IMG_0051"]];
    [image setFrame: CGRectMake(self.view.frame.size.width/2 - 100, self.view.frame.size.height/2 - 225, 200, 200)];
    // 使头像变为圆角
    [image.layer setCornerRadius:100];
    // 隐藏掉裁剪部分
    [image.layer setMasksToBounds:YES];
    
    [newPage addSubview:image];
    
    // 页面文字内容
    [newPage addSubview: [self anyLabel: 0 :self.view.frame.size.height /2 :self.view.frame.size.width :30 :@"姓名：Kuznetsov"]];
    [newPage addSubview: [self anyLabel: 0 :self.view.frame.size.height /2 + 30 :self.view.frame.size.width :30 :@"邮箱：kuznetsov@email.com"]];
    [newPage addSubview: [self anyLabel: 0 :self.view.frame.size.height /2 + 60 :self.view.frame.size.width :30 :@"电话：(123)456-7890"]];
    [newPage addSubview: [self anyLabel: 0 :self.view.frame.size.height /2 + 90 :self.view.frame.size.width :30 :@"版本：11.6"]];
    [newPage addSubview: [self anyLabel: 0 :self.view.frame.size.height /2 + 120 :self.view.frame.size.width :30 :@"清除缓存：2021年11月11日"]];
    [newPage addSubview: [self anyLabel: 0 :self.view.frame.size.height /2 + 150 :self.view.frame.size.width :30 :@"©️：CC BY-SA 3.0"]];
    
    // 页面添加背景颜色并显示
    [self.view addSubview: newPage];
}

- (UILabel *)anyLabel: (CGFloat) x: (CGFloat) y: (CGFloat) w: (CGFloat) h: (NSString *) label
{
    UILabel *myLabel = [[UILabel alloc] initWithFrame: CGRectMake(x, y, w, h)];
    [myLabel setText: label];
    [myLabel setFont: [UIFont systemFontOfSize: 20]];
    [myLabel setTextColor: [UIColor blackColor]];
    [myLabel setTextAlignment:NSTextAlignmentCenter];
    return myLabel;
}

@end

@implementation GradientLayer

- (instancetype)init
{
    self = [super init];
    if (self) {
        [self setNeedsDisplay];
    }
    return self;
}

- (void)drawInContext:(CGContextRef)ctx
{

    size_t gradLocationsNum = 2;
    CGFloat gradLocations[2] = {0.0f, 1.0f};
//    CGFloat gradColors[8] = {0.0f,0.0f,0.0f,0.0f,0.0f,0.0f,0.0f,0.5f};
    CGFloat gradColors[8] = {0.898039f,0.898039f,0.917647f,1.0f,1.0f,1.0f,0.0f,0.0f};
    CGColorSpaceRef colorSpace = CGColorSpaceCreateDeviceRGB();
    CGGradientRef gradient = CGGradientCreateWithColorComponents(colorSpace, gradColors, gradLocations, gradLocationsNum);
    CGColorSpaceRelease(colorSpace);

    CGPoint gradCenter= CGPointMake(CGRectGetMidX(self.bounds), CGRectGetMidY(self.bounds));
    CGFloat gradRadius = MIN(self.bounds.size.width , self.bounds.size.height) ;

    CGContextDrawRadialGradient (ctx, gradient, gradCenter, 0, gradCenter, gradRadius, kCGGradientDrawsAfterEndLocation);


    CGGradientRelease(gradient);
}

@end
