//
//  AddController.m
//  mosad_HW2
//
//  Created by 邵震东 on 2021/10/14.
//

#import <Foundation/Foundation.h>
#import "AddController.h"
#import "DiscoverController.h"

@interface AddController()
//@property (nonatomic, strong) UILabel *titleLabel;
//@property (nonatomic, strong) UILabel *anyLabel;
@property (nonatomic, strong) UITextField *tf1;
@property (nonatomic, strong) UITextField *tf2;
@property (nonatomic, strong) UITextField *tf3;
@property (nonatomic, strong)  UITextView *tf4;

@end

@implementation AddController

-(void)viewDidLoad
{
    [super viewDidLoad];
    
    [self.view setBackgroundColor: [UIColor systemBackgroundColor]];
    [self.view addSubview: [self setBackView]];
    
    // Labels
    [self.view addSubview: [self anyLabel: -120 :70 :self.view.frame.size.width :30 :@"日期"]];
    [self.view addSubview: [self anyLabel: -120 :130 :self.view.frame.size.width :30 :@"地点"]];
    [self.view addSubview: [self anyLabel: -120 :190 :self.view.frame.size.width :30 :@"搭配"]];
    [self.view addSubview: [self anyLabel: -120 :250 :self.view.frame.size.width :30 :@"心得"]];
    [self.view addSubview: [self anyLabel: -120 :350 :self.view.frame.size.width :30 :@"配图"]];
    
    // TextFields
//    [self.view addSubview: [self anyTextField: 120 :70 :200 :30 :@"2000-01-01"]];
    self.tf1 = [self anyTextField: 120 :70 :200 :30 :@"2000-01-01"];
    self.tf2 = [self anyTextField: 120 :130 :200 :30 :@"Shenzhen, Guangdong"];
    self.tf3 = [self anyTextField: 120 :190 :200 :30 :@"White shirt, blue jeans"];
    self.tf4 = [self anyTextView: 120 :250 :200 :90 :@"No value"];
    
    [self.view addSubview: self.tf1];
    [self.view addSubview: self.tf2];
    [self.view addSubview: self.tf3];
    [self.view addSubview: self.tf4];
    
    // Buttons
    [self.view addSubview: [self anyButton:120 :350 :90 :90 :@"+" :[UIColor darkGrayColor] :[UIColor systemGray6Color]]];
    UIButton *submit = [self anyButton:self.view.frame.size.width / 2 - 45 :500 :90 :50 :@"Submit" :[UIColor systemBlueColor] :[UIColor systemGray6Color]];
    [submit.layer setCornerRadius: 10];
    [submit addTarget: self action: @selector (btnClick:) forControlEvents: UIControlEventTouchUpInside];
    [self.view addSubview: submit];
}


- (void)btnClick: (UIButton *) btn
{
    // 使用单例传值构建通讯
    DataHandle *dataHandle = [DataHandle sharedDataHandle];
    
    // 构造新的 Cell
    DataCell *dc = [[DataCell alloc] init];
    dc.date = self.tf1.text;
    dc.lctn = self.tf2.text;
    dc.clth = self.tf3.text;
    dc.feel = self.tf4.text;
    
    // 将新的 Cell 追加在数组后面
    dataHandle.arrObj = [dataHandle.arrObj arrayByAddingObject: dc];
    
    // 实现弹窗并转移窗口
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"Success" message:@"Submit successfully completed" preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction *OKAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction *alert){
//        [self.navigationController popViewControllerAnimated:YES];
    }];
    [alert addAction: OKAction];
    [self presentViewController:alert animated:YES completion:nil];
    
    NSLog(@"Add successfully completed!");
    return;
}


- (UILabel *)anyLabel: (CGFloat) x: (CGFloat) y: (CGFloat) w: (CGFloat) h: (NSString *) label
{
    UILabel *myLabel = [[UILabel alloc] initWithFrame: CGRectMake(x, y, w, h)];
    [myLabel setText: label];
    [myLabel setFont: [UIFont systemFontOfSize: 20]];
    [myLabel setTextColor: [UIColor blackColor]];
    [myLabel setTextAlignment:NSTextAlignmentCenter];
    return myLabel;
}

- (UITextField *)anyTextField: (CGFloat) x: (CGFloat) y: (CGFloat) w: (CGFloat) h: (NSString *) text
{
    UITextField *myText = [[UITextField alloc] initWithFrame: CGRectMake(x, y, w, h)];
    [myText setBackgroundColor: [UIColor whiteColor]];
    [myText setPlaceholder: text];
    // 为验收之便，将其设为 text
//    [myText setText: text];
    [myText setTextColor: [UIColor blackColor]];
    [myText setBorderStyle: UITextBorderStyleRoundedRect];
    return myText;
}

- (UITextView *)anyTextView: (CGFloat) x: (CGFloat) y: (CGFloat) w: (CGFloat) h: (NSString *) text
{
    UITextView *myText = [[UITextView alloc] initWithFrame: CGRectMake(x, y, w, h)];
    myText.layer.cornerRadius = 5;
    myText.layer.masksToBounds = YES;
    myText.layer.borderColor = [UIColor systemGray5Color].CGColor;
    myText.layer.borderWidth = 1;
    [myText setBackgroundColor: [UIColor whiteColor]];
    [myText setText: text];
    [myText setIndicatorStyle: UITextBorderStyleRoundedRect];
    return myText;
}

- (UIButton *)anyButton: (CGFloat) x: (CGFloat) y: (CGFloat) w: (CGFloat) h: (NSString *) text: (UIColor*) textColor: (UIColor *) backColor
{
    UIButton *btn = [UIButton buttonWithType: UIButtonTypeRoundedRect];
    [btn setFrame: CGRectMake(x, y, w, h)];
    [btn setTitleColor:textColor forState:UIControlStateNormal];
    [btn setBackgroundColor: backColor];
    [btn setTitle:text forState:UIControlStateNormal];
    return btn;
}

- (UIView *) setBackView
{
    // 初始化背景
    UIView *backView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, [[UIScreen mainScreen] bounds].size.width, self.view.frame.size.height)];
//    [self.view addSubview:backView];
    //初始化CAGradientlayer对象，使它的大小为UIView的大小
    CAGradientLayer *gradientLayer = [CAGradientLayer layer];
    gradientLayer.frame = backView.bounds;
    //将CAGradientlayer对象添加在我们要设置背景色的视图的layer层
    [backView.layer addSublayer:gradientLayer];
    [gradientLayer setCornerRadius: 1];
    //设置渐变区域的起始和终止位置（范围为0-1）
    //设置（1，0）为横向，设置（0，1）为纵向，设置（1，1）为对角方向
    gradientLayer.startPoint = CGPointMake(1, 1);
    gradientLayer.endPoint = CGPointMake(1, 0);
    //设置颜色数组
//    [UIColor colorWithRed:(float)(250/255.0f) green:(float)(250/255.0f) blue:(float)(195/255.0f) alpha:1.0f];
//    gradientLayer.colors = @[(__bridge id)[UIColor whiteColor].CGColor, (__bridge id)[UIColor systemGray5Color].CGColor];
    gradientLayer.colors = @[(__bridge id)[UIColor whiteColor].CGColor, (__bridge id)[UIColor colorWithRed:(float)(250/255.0f) green:(float)(250/255.0f) blue:(float)(195/255.0f) alpha:1.0f].CGColor];
    //设置颜色分割点（范围：0-1）
    gradientLayer.locations = @[@(0.0f), @(1.0f)];
    return backView;
}

@end
