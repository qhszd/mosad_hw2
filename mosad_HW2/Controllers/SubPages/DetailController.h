//
//  DetailController.h
//  mosad_HW2
//
//  Created by on 2021/10/21.
//

#ifndef DetailController_h
#define DetailController_h
#import <UIKit/UIKit.h>
#import "../../Layout/DataCell.h"

@interface DetailController : UIViewController
@property (nonatomic, strong) DataCell *cell;
@end

#endif /* DetailController_h */
