//
//  DetailController.m
//  mosad_HW2
//
//  Created by 邵震东 on 2021/10/21.
//

#import <Foundation/Foundation.h>
#import "DetailController.h"

@interface DetailController()

@end

@implementation DetailController

- (void) viewDidLoad
{
    [super viewDidLoad];
    [self.view addSubview: [self setBackView]];
    self.title = @"Details";
    
    [self.view addSubview: [self anyLabel: -120 :70 :self.view.frame.size.width :30 :@"日期"]];
    [self.view addSubview: [self anyLabel: -120 :130 :self.view.frame.size.width :30 :@"地点"]];
    [self.view addSubview: [self anyLabel: -120 :190 :self.view.frame.size.width :30 :@"搭配"]];
    [self.view addSubview: [self anyLabel: -120 :250 :self.view.frame.size.width :30 :@"心得"]];
    [self.view addSubview: [self anyLabel: -120 :350 :self.view.frame.size.width :30 :@"配图"]];
    
    // TextFields
//    [self.view addSubview: [self anyTextField: 120 :70 :200 :30 :@"2000-01-01"]];
    [self.view addSubview: [self anyTextField: 120 :70 :200 :30 : self.cell.date]];
    [self.view addSubview: [self anyTextField: 120 :130 :200 :30 : self.cell.lctn]];
    [self.view addSubview: [self anyTextField: 120 :190 :200 :30 : self.cell.clth]];
    [self.view addSubview: [self anyTextView: 120 :250 :200 :90 : self.cell.feel]];
    
    [self.view addSubview: [self anyButton:120 :350 :90 :90 :@"+" :[UIColor darkGrayColor] :[UIColor systemGray6Color]]];
}

- (UILabel *)anyLabel: (CGFloat) x: (CGFloat) y: (CGFloat) w: (CGFloat) h: (NSString *) label
{
    UILabel *myLabel = [[UILabel alloc] initWithFrame: CGRectMake(x, y, w, h)];
    [myLabel setText: label];
    [myLabel setFont: [UIFont systemFontOfSize: 20]];
    [myLabel setTextColor: [UIColor blackColor]];
    [myLabel setTextAlignment:NSTextAlignmentCenter];
    return myLabel;
}

- (UIButton *)anyButton: (CGFloat) x: (CGFloat) y: (CGFloat) w: (CGFloat) h: (NSString *) text: (UIColor*) textColor: (UIColor *) backColor
{
    UIButton *btn = [UIButton buttonWithType: UIButtonTypeRoundedRect];
    [btn setFrame: CGRectMake(x, y, w, h)];
    [btn setTitleColor:textColor forState:UIControlStateNormal];
    [btn setBackgroundColor: backColor];
    [btn setTitle:text forState:UIControlStateNormal];
    return btn;
}

- (UITextField *)anyTextField: (CGFloat) x: (CGFloat) y: (CGFloat) w: (CGFloat) h: (NSString *) text
{
    UITextField *myText = [[UITextField alloc] initWithFrame: CGRectMake(x, y, w, h)];
    [myText setBackgroundColor: [UIColor whiteColor]];
    [myText setText: text];
    [myText setTextColor: [UIColor blackColor]];
    [myText setBorderStyle: UITextBorderStyleRoundedRect];
    return myText;
}

- (UITextView *)anyTextView: (CGFloat) x: (CGFloat) y: (CGFloat) w: (CGFloat) h: (NSString *) text
{
    UITextView *myText = [[UITextView alloc] initWithFrame: CGRectMake(x, y, w, h)];
    myText.layer.cornerRadius = 5;
    myText.layer.masksToBounds = YES;
    myText.layer.borderColor = [UIColor systemGray5Color].CGColor;
    myText.layer.borderWidth = 1;
    [myText setBackgroundColor: [UIColor whiteColor]];
    [myText setText: text];
    [myText setIndicatorStyle: UITextBorderStyleRoundedRect];
    return myText;
}

- (UIView *) setBackView
{
    // 初始化背景
    UIView *backView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, [[UIScreen mainScreen] bounds].size.width, self.view.frame.size.height)];
//    [self.view addSubview:backView];
    //初始化CAGradientlayer对象，使它的大小为UIView的大小
    CAGradientLayer *gradientLayer = [CAGradientLayer layer];
    gradientLayer.frame = backView.bounds;
    //将CAGradientlayer对象添加在我们要设置背景色的视图的layer层
    [backView.layer addSublayer:gradientLayer];
    [gradientLayer setCornerRadius: 1];
    //设置渐变区域的起始和终止位置（范围为0-1）
    //设置（1，0）为横向，设置（0，1）为纵向，设置（1，1）为对角方向
    gradientLayer.startPoint = CGPointMake(0, 0);
    gradientLayer.endPoint = CGPointMake(1, 1);
    //设置颜色数组
//    [UIColor colorWithRed:(float)(250/255.0f) green:(float)(250/255.0f) blue:(float)(195/255.0f) alpha:1.0f];
//    gradientLayer.colors = @[(__bridge id)[UIColor whiteColor].CGColor, (__bridge id)[UIColor systemGray5Color].CGColor];
    gradientLayer.colors = @[(__bridge id)[UIColor whiteColor].CGColor, (__bridge id)[UIColor colorWithRed:(float)(250/255.0f) green:(float)(250/255.0f) blue:(float)(195/255.0f) alpha:1.0f].CGColor];
    //设置颜色分割点（范围：0-1）
    gradientLayer.locations = @[@(0.0f), @(1.0f)];
    return backView;
}

@end
