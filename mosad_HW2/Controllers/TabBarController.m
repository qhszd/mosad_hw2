//
//  ViewController.m
//  mosad_HW2
//
//  Created by 邵震东 on 2021/10/13.
//

#import "TabBarController.h"
#import "DiscoverController.h"
#import "AddController.h"
#import "UserController.h"

@interface TabBarController()
@end

@implementation TabBarController

-(void)viewDidLoad {
    [super viewDidLoad];
    
    // 创建三个子页面
    DiscoverController *vc1 = [[DiscoverController  alloc] init];
         AddController *vc2 = [[AddController       alloc] init];
        UserController *vc3 = [[UserController      alloc] init];
    
    // 在三个子页面上添加 NavigationController 负责显示标题
    UINavigationController *nc1 = [[UINavigationController alloc] initWithRootViewController: vc1];
    UINavigationController *nc2 = [[UINavigationController alloc] initWithRootViewController: vc2];
    UINavigationController *nc3 = [[UINavigationController alloc] initWithRootViewController: vc3];
    
    // 自定义 title
    vc1.title = @"Collocations";
    vc2.title = @"Add Collocation";
    vc3.title = @"My Account";
    
    // 自定义底部栏 tag
    nc1.tabBarItem.title = @"Discover";
    nc2.tabBarItem.title = @"Record";
    nc3.tabBarItem.title = @"Account";
    
    // 给 tabBar 添上图片
    nc1.tabBarItem.image = [UIImage systemImageNamed:@"book"];
    nc2.tabBarItem.image = [UIImage systemImageNamed:@"pencil"];
    nc3.tabBarItem.image = [UIImage systemImageNamed:@"person"];
    // 选中 tag 后显示的图片
    nc1.tabBarItem.selectedImage = [UIImage systemImageNamed:@"book.fill"];
    nc3.tabBarItem.selectedImage = [UIImage systemImageNamed:@"person.fill"];
    
    // 将以上三个 Nav 对象创建为数组并写入 ViewController
    NSArray *vcArr = [NSArray arrayWithObjects: nc1, nc2, nc3, nil];
    self.viewControllers = vcArr;
}

@end
