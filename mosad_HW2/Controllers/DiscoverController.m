//
//  DiscoverController.m
//  mosad_HW2
//
//  Created by 邵震东 on 2021/10/14.
//

#import <Foundation/Foundation.h>
#import "DiscoverController.h"
#import "../Layout/DataHandle.h"
#import "SubPages/DetailController.h"


@interface DiscoverController()
@property (nonatomic, retain) UITableView *tableView;
@property (nonatomic, retain) NSMutableArray *arrayData;
@end


@implementation DiscoverController


-(void)viewDidLoad
{
    [super viewDidLoad];
    
    // 初始化背景
    UIView *backView = [self setBackView];
    
    _arrayData = [NSMutableArray array];
    
    for ( int i = 'A'; i < 'Z'; ++i )
    {
        NSMutableArray *arrSmall = [[NSMutableArray alloc] init];
        for ( int j = 1; j < 5; ++j )
        {
            NSString *str = [NSString stringWithFormat: @"%c%d", i, j];
            [arrSmall addObject: str];
        }
        [_arrayData addObject:arrSmall];
    }
    
    // 建立单例传值系统以实现页面间通信
    DataHandle *dataHandle = [DataHandle sharedDataHandle];
    NSLog(@"arrObj num: %ld", [dataHandle.arrObj count]);
    [[dataHandle.arrObj objectAtIndex: 0] setClth: @"Yello shirt, blue jeans"];
    
    self.tableView = [[UITableView alloc] initWithFrame:self.view.bounds style:UITableViewStyleInsetGrouped];
    self.tableView.delegate = self;
    self.tableView.dataSource = self;
    [self.tableView registerClass: [UITableViewCell class] forCellReuseIdentifier: @"reuse"];
    self.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;       // 隐藏 TableView 的分割线
    self.tableView.backgroundView = backView;                                // 为 TableView 添加背景
    UISearchBar *srch = [[UISearchBar alloc] initWithFrame: CGRectMake(0, 0, self.tableView.frame.size.width, 60)];
    [srch setPlaceholder: @"Type to Search"];
    [srch setReturnKeyType: UIReturnKeySearch];
    [self.tableView setTableHeaderView: srch];
    [self.view addSubview: self.tableView];
}

- (void)viewDidAppear:(BOOL)animated
{
    [self.tableView reloadData];
    return;
}


# pragma mark - 设置 TableView 的各种属性

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    DataHandle *dataHandle = [DataHandle sharedDataHandle];
    return [dataHandle.arrObj count];
//    return 3;
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return 3;
}


- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section
{
    NSString *str = [NSString stringWithFormat: @"穿搭%ld", section + 1];
    return str;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"reuse" forIndexPath:indexPath];
    DataHandle *dataHandle = [DataHandle sharedDataHandle];
    NSLog(@"section index: %ld", indexPath.section);
    
    DataCell *this = dataHandle.arrObj[indexPath.section];
    NSLog(@"CHECKPOINT1!");
    if ( indexPath.row == 0 ) {
        NSString *str = [NSString stringWithFormat: @"日期：%@", this.date];
        cell.textLabel.text = str;
    } else if ( indexPath.row == 1 ) {
        NSString *str = [NSString stringWithFormat: @"地点：%@", this.lctn];
        cell.textLabel.text = str;
    } else {
        NSString *str = [NSString stringWithFormat: @"穿搭：%@", this.clth];
        cell.textLabel.text = str;
    }
    return cell;
}

// 设置进入事件
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    DataHandle *dataHandle = [DataHandle sharedDataHandle];
    DetailController *dc = [[DetailController alloc] init];
    dc.cell = [dataHandle.arrObj objectAtIndex: indexPath.section];
    self.hidesBottomBarWhenPushed = YES;
    [self.navigationController pushViewController:dc animated:YES];
    self.hidesBottomBarWhenPushed = NO;
}

- (UIView *) setBackView
{
    // 初始化背景
    UIView *backView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, [[UIScreen mainScreen] bounds].size.width, self.view.frame.size.height)];
//    [self.view addSubview:backView];
    //初始化CAGradientlayer对象，使它的大小为UIView的大小
    CAGradientLayer *gradientLayer = [CAGradientLayer layer];
    gradientLayer.frame = backView.bounds;
    //将CAGradientlayer对象添加在我们要设置背景色的视图的layer层
    [backView.layer addSublayer:gradientLayer];
    [gradientLayer setCornerRadius: 1];
    //设置渐变区域的起始和终止位置（范围为0-1）
    //设置（1，0）为横向，设置（0，1）为纵向，设置（1，1）为对角方向
    gradientLayer.startPoint = CGPointMake(0, 0);
    gradientLayer.endPoint = CGPointMake(1, 1);
    //设置颜色数组
//    [UIColor colorWithRed:(float)(250/255.0f) green:(float)(250/255.0f) blue:(float)(195/255.0f) alpha:1.0f];
//    gradientLayer.colors = @[(__bridge id)[UIColor whiteColor].CGColor, (__bridge id)[UIColor systemGray5Color].CGColor];
    gradientLayer.colors = @[(__bridge id)[UIColor whiteColor].CGColor, (__bridge id)[UIColor colorWithRed:(float)(250/255.0f) green:(float)(250/255.0f) blue:(float)(195/255.0f) alpha:1.0f].CGColor];
    //设置颜色分割点（范围：0-1）
    gradientLayer.locations = @[@(0.0f), @(1.0f)];
    return backView;
}

@end
